<?php
/**
* Expose context condition.
*/
class context_content_profile_user_profile_type_context_condition extends context_condition_node {
  function condition_values() {
    return context_content_profile_get_types();
  }

  function execute($type, $op) {
    foreach ($this->get_contexts($type) as $context) {
      // Check the node form option.
      $options = $this->fetch_from_context($context, 'options');
      if ($op === 'form') {
        if (!empty($options['node_form']) && in_array($options['node_form'], array(1, 2))) {
          $this->condition_met($context, $type);
        }
      }
      elseif (empty($options['node_form']) || $options['node_form'] != 2) {
        $this->condition_met($context, $type);
      }
    }
  }
}
?>